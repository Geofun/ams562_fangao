#include "manip.hpp"

#include <algorithm>
#include <cmath>

// you will need cmath and algorithm headers.

namespace ams562_final {

void compute_n2e_adj(const unsigned n, const Triangles &conn,
                     std::vector<std::vector<int>> &adj) {
  // resize adj to n
  adj.resize(n);
  // reserve for each of them with a reasonable upper bound

  for(int index=0; index<(int)conn.ntris(); index++)
  {
    auto& tri = conn[index];
    adj[tri[0]].push_back(index);
    adj[tri[1]].push_back(index);
    adj[tri[2]].push_back(index);
  }

  // your code
}

void compute_avg_normals(const SphCo &points, const Triangles &conn,
                         const std::vector<std::vector<int>> &n2e_adj,
                         SphCo &nrms) {
  // resize the nrms
  nrms.resize(points.npoints());

  
  SphCo f_nrms;
  f_nrms.resize(conn.ntris());
  for(int index=0; index<(int)conn.ntris(); index++)
  {
    auto&& tri = conn[index];
    auto& A = points[tri[0]];
    auto& B = points[tri[1]];
    auto& C = points[tri[2]];
    std::array<double, 3> AB;
    std::array<double, 3> AC;
    auto& l = (AB[0] = B[0]-A[0]);
    auto& m = (AB[1] = B[1]-A[1]);
    auto& n = (AB[2] = B[2]-A[2]);

    auto& o = (AC[0] = C[0]-A[0]);
    auto& p = (AC[1] = C[1]-A[1]);
    auto& q = (AC[2] = C[2]-A[2]);

    f_nrms[index][0] = m*q-n*p;
    f_nrms[index][1] = n*o-l*q;
    f_nrms[index][2] = l*p-m*o;
  }
  f_nrms.normalize();


  for(int index=0; index<(int)points.npoints(); index++)
  {
    int count = n2e_adj[index].size();
    double sum_x = 0;
    double sum_y = 0;
    double sum_z = 0;   
    for(int i=0; i<count; i++)
    {
      sum_x += f_nrms[ n2e_adj[index][i] ][0];
      sum_y += f_nrms[ n2e_adj[index][i] ][1];
      sum_z += f_nrms[ n2e_adj[index][i] ][2];
    } 
    nrms[index][0] = sum_x/count;
    nrms[index][1] = sum_y/count;
    nrms[index][2] = sum_z/count;
  }
  nrms.normalize();


  // your code

  // hint don't forget normalizing the normals
}

void compute_errors(const SphCo &exact, const SphCo &num,
                    std::vector<double> &acos_theta) {
  // resize the error array
  acos_theta.resize(num.npoints());
  for(int index=0; index<(int)num.npoints(); index++)
  {
    acos_theta[index] = std::acos(
      exact[index][0]*num[index][0]
      +
      exact[index][1]*num[index][1]
      +
      exact[index][2]*num[index][2]
    );
  }

  // your code
}

}  // namespace ams562_final