// This is the source file that contains the implementation of
// CSRMatrix and its corresponding functions

#include "csr.hpp"
#include "vec.hpp"

#include <iostream>

namespace csr {

// impls

// create a csr matrix
CSRMatrix *create(const int n, const int nnz) {
  CSRMatrix *ptr = nullptr;
  // FINISH ME
  if (n <= 0||nnz<=0) {
	  // invalid size, return nullptr
	  return ptr;
  }
  // allocation
  ptr = new CSRMatrix;
  if (!ptr) {
	  return nullptr;
  }
  ptr->value = nullptr;
  ptr->indices= nullptr;
  ptr->indptr = nullptr;
  ptr->n = n;
  // FINISH ME
  ptr->value = new double[nnz];
  ptr->indices = new int[nnz];
  ptr->indptr = new int[n+1];
  ptr->indptr[0] = 0;
  if (!ptr->value || !ptr->indices || !ptr->indptr) {
	  // value is failed to be initialized, but ptr is already allocated.
	  // So a cleanup is needed.
	  delete ptr;
	  return nullptr;
  }
  return ptr;
}

// destroy a csr matrix
void destroy(CSRMatrix *mat) {
  if (!mat) {
    return;
  }
  // FINISH ME
  if (!mat->value) {
	  delete mat;
	  mat = nullptr;
	  return;
  }
  // deallocation
  delete[] mat->value;
  delete[] mat->indices;
  delete[] mat->indptr;
  delete mat;
  mat = nullptr;
  // FINISH ME
}

// assign a row
bool assign_row(CSRMatrix &mat, const int row, const int *cols,
                const double *vals, const int nnz) {
  if (row < 0 || row > mat.n || nnz < 0) return true;
  const int start = mat.indptr[row];  // bonus, this is how to get the starting entry of this row
  bool fail = false;
  // FINISH ME
  // construct row
  mat.indptr[row + 1] = start + nnz;
  for (int k = start; k < start + nnz; ++k) {
	  mat.indices[k] = cols[k - start];
	  mat.value[k] = vals[k - start];
  }
  // FINISH ME
  return fail;
}

// extract the diagonal values
bool extract_diag(const CSRMatrix &A, vec::DenseVec &diag) {
  if (A.n != diag.n) return true;
  bool fail = false;
  // FINISH ME
  // extracting diagonal entries
  for (int k = 0; k < A.n; ++k) {
	  for (int kk = A.indptr[k]; kk < A.indptr[k+1]; ++kk) {
		  if (k == A.indices[kk]) {
			  diag.value[k] = A.value[kk];
		  }
	  }
  }
  // FINISH ME
  return fail;
}

// matrix vector multiplication
bool mv(const CSRMatrix &A, const vec::DenseVec &x, vec::DenseVec &y) {
  bool fail = false;
  // FINISH ME
  // matrix vector multiplication
  for (int i = 0; i <A.n ; i++){
	  y.value[i] = 0;
  }
  for (int k = 0; k < A.n; ++k) {
	  for (int kk = A.indptr[k]; kk < A.indptr[k + 1]; ++kk) {
		  y.value[k] += A.value[kk] * x.value[A.indices[kk]];
	  }
  }
  // FINISH ME
  return fail;
}

}  // namespace csr
