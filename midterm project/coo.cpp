// source code of COO format

#include "coo.hpp"
#include "vec.hpp"

namespace coo {

// create a coo matrix
COOMatrix *create(const int n, const int nnz) {
  COOMatrix *ptr = nullptr;
  // FINISH ME
  if (n <= 0||nnz<=0) {
	  // invalid size, return nullptr
	  return ptr;
  }
  // allocate ptr
  ptr = new COOMatrix;
  if (!ptr) {
	  return nullptr;
  }
  ptr->v = nullptr;
  ptr->i = nullptr;
  ptr->j= nullptr;
  ptr->n = n;
  ptr->nnz = nnz;
  // FINISH ME
  ptr->v = new double[nnz];
  ptr->i = new int[nnz];
  ptr->j = new int[nnz];
  if (!ptr->v|| !ptr->i|| !ptr->j) {
	  // value is failed to be initialized, but ptr is already allocated.
	  // So a cleanup is needed.
	  delete ptr;
	  return nullptr;
  }
  return ptr;
}

// destroy a csr matrix
void destroy(COOMatrix *mat) {
  if (!mat) return;
  // FINISH ME
  if (!mat->v) {
	  delete mat;
	  mat = nullptr;
	  return;
  }
  // deallocate ptr
  delete[] mat->v;
  delete[] mat->i;
  delete[] mat->j;
  delete mat;
  mat = nullptr;
  // FINISH ME
}

// assign a triplet (i,j,v)
bool assign_ijv(COOMatrix &mat, const int i, const int j, const double v,
                const int nnz_index) {
  bool fail = false;
  // FINISH ME
  // construct entry (i,j,v) in slot nnz_index
  mat.i[nnz_index] = i;
  mat.j[nnz_index] = j;
  mat.v[nnz_index] = v;
  // FINISH ME
  return fail;
}

// extract the diagonal values
bool extract_diag(const COOMatrix &A, vec::DenseVec &diag) {
  if (A.n != diag.n) return true;
  bool fail = false;
  // FINISH ME
  // extracting diagonal entries
  for (int k = 0; k < A.nnz; ++k) {
	  if (A.i[k] == A.j[k]) {
		  diag.value[A.i[k]] = A.v[k];
	  }
  }
  // FINISH ME
  return fail;
}

// matrix vector multiplication
bool mv(const COOMatrix &A, const vec::DenseVec &x, vec::DenseVec &y) {
  bool fail = false;
  // FINISH ME
  // matrix vector multiplication
  for (int i = 0; i < A.n; i++) {
	  y.value[i] = 0;
  }
  const int size = A.nnz;
   for (int k=0; k<size ; ++k){
	  y.value[A.i[k]] += A.v[k] * x.value[A.j[k]];
  }
  // FINISH ME
  return fail;
}

}  // namespace coo
