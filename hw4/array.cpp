#include <iostream>
#include <cmath>
#include<algorithm>
#include "array.hpp"

using namespace std;

Array::Array() {
    _size = 1;
	_data=new double[_size];
}

Array::Array(const Array &other) {
	_size = other._size;
	_data = new double[_size];
	for (unsigned k = 0; k < _size; k++) {
		_data[k] = other._data[k];
	}
}

Array::Array(unsigned n, double v) {
	_size = n;
	_data = new double[_size];
	for (unsigned k = 0; k < _size; k++) {
		_data[k] = v;
	}
}

Array::~Array() {
	delete[] _data;
	_data= NULL;
}

unsigned Array::size() const {
	return _size;
}

double *Array::data() {
	return _data;
}

const double * Array::data() const{
	return _data;
}

void Array::copy(const Array & rhs){
	_size = rhs._size;
    _data = new double[_size];
	for (unsigned k = 0; k < _size; k++) {
		_data[k] = rhs._data[k];
	}
}

double & Array::at(unsigned index){
	return _data[index];
}

const double & Array::at(unsigned index) const
{
	return _data[index];
}

void Array::resize(unsigned new_size, bool prsv){
	double *new_data;
	new_data = new double[new_size];
	if (prsv == true) {
		for (unsigned k=0; k <std::min(new_size,_size); k++) {
			new_data[k] = _data[k];
		}
	}
	delete[] _data;
	_data = new_data;
}

double Array::norm() const{
	double nor=0;
	for (unsigned k = 0; k < _size; k++) {
		nor += (_data[k])*(_data[k]);
	}
	nor = sqrt(nor);
	std::cout <<nor<< std::endl;
	return nor ;
}

double Array::sum() const{
	double su = 0;
	for (unsigned k = 0; k < _size; k++) {
		su += _data[k];
	}
	std::cout << su << std::endl;
	return su;
}

double Array::max() const{
	double ma = 0;
	for (unsigned k = 0; k < _size; k++) {
		ma = std::max(_data[k],ma);
	}
	std::cout << ma << std::endl;
	return ma;
}

double Array::min() const{
	double mi = INFINITY;
	for (unsigned k = 0; k < _size; k++) {
		mi = std::min(_data[k], mi);
	}
	std::cout << mi << std::endl;
	return mi;
}

double Array::dot(const Array & rhs) const{
	double dt = 0;
	for (unsigned k = 0; k < _size; k++) {
		dt += (rhs._data[k])*(rhs._data[k]);
	}
	std::cout << dt << std::endl;
	return dt;
}

Array Array::add(const Array & rhs) const{
	for (unsigned k = 0; k < _size; k++) {
		std::cout << _data[k]+rhs._data[k] << ' ';
	}
	std::cout << std::endl;
	return Array(_size,*_data+*rhs._data);
}

Array Array::sub(const Array & rhs) const{
	for (unsigned k = 0; k < _size; k++) {
		std::cout << _data[k] - rhs._data[k] << ' ';
	}
	std::cout << std::endl;
	return Array(_size, *_data - *rhs._data);
}

void Array::print() const{
		for (unsigned k = 0; k < _size; k++) {
			std::cout << _data[k]<<' ';
		}
		std::cout << std::endl;
}




