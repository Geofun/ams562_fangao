#include <iostream>
#include <cmath>
#include<algorithm>
#include "array.hpp"

using namespace std;

int main(){
	std::cout << "Create Array A(1.1,1.1) and print it" << std::endl;
	Array A(2,1.1);
	A.print();
	std::cout << "change A.data[0]=3.3" << std::endl;
	*A.data() = 3.3;
	std::cout <<  "now A.data is" << std::endl;
	A.print();
	std::cout << "Create Array B (2.2,2.2) and print it" << std::endl;
	Array B(2, 2.2);
	B.print();
	std::cout << "Copy A to B and print B" << std::endl;
	B.copy(A);
	B.print();
	std::cout << "A(3.3,1.1)+B(3.3,1,1) and print it" << std::endl;
	B.add(A);
	std::cout << "A(3.3,1.1)-B(3.3,1,1) and print it" << std::endl;
	B.sub(A);
	std::cout << "calculate the norm, max, min, sum ,dot of A(3.3,1.1)" << std::endl;
	A.norm();
	A.max();
	A.min();
	A.sum();
	A.dot(A);









	system("pause");
	return 0;
}
