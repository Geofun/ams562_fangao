#include <iostream>
#include <fstream>
#include <streambuf>  
#include "rand.hpp"
#include "matrix.hpp"
#include<vector>

int main() {
  ams562::RandGen gen;
  std::cout << "test to random generate matrix twice,1st: "<<gen.generate()<<" 2nd: " <<gen.generate() << '\n';

  std::cout << "test to input matrix{ {1,2,3},{4,5,6},{7,8,9} }" << std::endl;
  std::vector<std::vector<double>> matrix = { {1,2,3},{4,5,6},{7,8,9} };
  int m = matrix.size();
  int n = matrix[0].size();
  for (int i = 0; i < m; i++) {
	  for (int j = 0; j < n; j++) {
		  std::cout << matrix[i][j] << " ";
	  }
	  std::cout << std::endl;
  }

  std::cout << "test to input a random 2*2 matrix" << std::endl;
  m = 2;
  n = 2;
  std::vector<std::vector<double>> matrix2(m);
  for (int i = 0; i < m; i++) {
	  matrix2[i].resize(n);
  }
  for (int i = 0; i < m; i++) {
	  for (int j = 0; j < n; j++) {
		  matrix2[i][j] = gen.generate();
	  }
  }
  for (int i = 0; i < m; i++) {
	  for (int j = 0; j < n; j++) {
		  std::cout << matrix2[i][j] << " ";
	  }
	  std::cout << std::endl;
  }

  std::cout << "test to input a matrix from test1.txt" << std::endl;
  std::ifstream infile;
  infile.open("test1.txt");
  infile >> m;
  infile >> n;
  std::vector<std::vector<double>> matrixtest1(m);
  for (int i = 0; i < m; i++) {
	  matrixtest1[i].resize(n);
  }
  for (int i = 0; i < m; i++) {
	  for (int j = 0; j < n; j++) {
		  infile >> matrixtest1[i][j];
	  }
  }
  for (int i = 0; i < m; i++) {
	  for (int j = 0; j < n; j++) {
		  std::cout << matrixtest1[i][j] << " ";
	  }
	  std::cout << std::endl;
  }
  infile.close();

  std::cout << "test to input a matrix from test2.txt" << std::endl;
  infile.open("test2.txt");
  infile >> m;
  infile >> n;
  std::vector<std::vector<double>> matrixtest2(m);
  for (int i = 0; i < m; i++) {
	  matrixtest2[i].resize(n);
  }
  for (int i = 0; i < m; i++) {
	  for (int j = 0; j < n; j++) {
		  infile >> matrixtest2[i][j];
	  }
  }
  for (int i = 0; i < m; i++) {
	  for (int j = 0; j < n; j++) {
		  std::cout << matrixtest2[i][j] << " ";
	  }
	  std::cout << std::endl;
  }
  infile.close();

  std::cout << "create a test3.txt" << std::endl;
  std::ofstream outfile("test3.txt");
  std::cout << "input a 3*3 matrix with all entries=1 in test3.txt" << std::endl;
  outfile << 3 << " ";
  outfile << 3 << " ";
  for (int i = 0; i < 9; i++) {
		 outfile << 1<<" ";
  }
  outfile.close();

  std::cout << "open test3.txt and output the data" << std::endl;
  infile.open("test3.txt");
  infile >> m;
  infile >> n;
  std::vector<std::vector<double>> matrixtest3(m);
  for (int i = 0; i < m; i++) {
	  matrixtest3[i].resize(n);
  }
  for (int i = 0; i < m; i++) {
	  for (int j = 0; j < n; j++) {
		  infile >> matrixtest3[i][j];
	  }
  }
  for (int i = 0; i < m; i++) {
	  for (int j = 0; j < n; j++) {
		  std::cout << matrixtest3[i][j] << " ";
	  }
	  std::cout << std::endl;
  }
  infile.close();



  system("pause");
  return 0;
}
