#include "pch.h"
#include <cmath>
#include <cstdlib>
#include <iostream>

int main(int argc, char *argv[]) {
	const double pi_4 = 0.25 * 3.141592653589793;
	double h = 0.0;

	h = std::atof(argv[1]);// get h from command line
	const double sinx = std::sin(pi_4);
	const double sinxh = std::sin(pi_4+h);
	const double sinxmh = std::sin(pi_4 -h);
	const double cosx = std::cos(pi_4);

	double ydh = (sinxh - sinx) / h; //compute forward difference

	double yd2h = (sinxh - sinxmh) / (2 * h);// compute center difference

	double err1 = ydh - cosx;// compare the errors
	double err2 = yd2h - cosx;

							 
	std::cout <<"first scheme error is"<< err1 << std::endl;// print results
	std::cout << "second scheme error is" << err2 << std::endl;

	return 0;
}
