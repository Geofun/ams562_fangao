#include "pch.h"
#include <cmath>
#include <cstdlib> 
#include <ctime>  // for trigger the seed of random number generator
#include <iostream>

static void genPointsOnUnitSphere(const int N, float *x, float *y, float *z);

int main(int argc, char *argv[]) {
	int N = -1;
	// get the size of N
	N = atof(argv[1]);

	float *x = nullptr, *y = nullptr, *z = nullptr;
	// allocate memory for x, y, z
	x = new float[N];
	y = new float[N];
	z = new float[N];
	genPointsOnUnitSphere(N, x, y, z);

	// determine the extreme arc lengths
	float arcmax = 0;
	float arcmin = 100;
	int pmax = 100;
	int pmin = 100;
	for (int i = 1; i < N; ++i) {
		const float arclen = std::acos(x[0] * x[i] + y[0] * y[i] + z[0] * z[i]);
		if (arclen > arcmax) { 
			arcmax = arclen;
			pmax = i;
		}
		if (arclen < arcmin) {
			arcmin=arclen;
			pmin = i;
		}	
	}
	std::cout << "maximal arc index is " << pmax << std::endl;
	std::cout << "maximal length is " << arcmax << std::endl;
	std::cout << "minimal arc index is " << pmin << std::endl;
	std::cout << "minimal length is " << arcmin << std::endl;
	// relax memory
	delete[] x;
	delete[] y;
	delete[] z;
	return 0;
}

void genPointsOnUnitSphere(const int N, float *x, float *y, float *z) {
	if (x == nullptr || y == nullptr || z == nullptr) {
		std::cerr << "invalid pointers, aborting...\n";
		std::exit(1);
	}
	std::srand(std::time(0));  // trigger the seed
	const int BD = 10000000, BD2 = 2 * BD;
	const float inv_bd = 1.0f / BD;  // not integer division
	for (int i = 0; i < N; ++i) {
		x[i] = (std::rand() % BD2 - BD) * inv_bd;
		y[i] = (std::rand() % BD2 - BD) * inv_bd;
		z[i] = (std::rand() % BD2 - BD) * inv_bd;
		const float len = std::sqrt(x[i] * x[i] + y[i] * y[i] + z[i] * z[i]);
		if (len > 1e-5f) {
			// project on to unit sphere
			x[i] /= len;
			y[i] /= len;
			z[i] /= len;
		}
		else {
			// too close to zero
			// directly set the point to be (1,0,0)
			x[i] = 1.0f;
			y[i] = z[i] = 0.0f;
		}
	}
}