#include "pch.h"
#include <cmath>    // sin
#include <cstdlib>  // atoi, atof
#include <iostream>

// the interface of the function is:
//  inputs:
//    a       left limit
//    b       right limit
//    maxit   maximal iteration allowed
//  outputs:
//    root    the root of the function
//    iters   iterations
//  return:
//    true if a root is found, false ew
// prototypes
bool bisecSin(const double a, const double b, const int maxit, double &root,
	int &iters);
bool bisecQuadratic(const double a, const double b, const int maxit,
	double &root, int &iters);
int main(int argc, char *argv[]) {
	std::cout << "input string a,b,maxit"<< std::endl;
	double a, b, root;
	int maxit, iters;

	// parse command line inputs
	a = atof(argv[1]);
	b = atof(argv[2]);
	maxit = atoi(argv[3]);

	// call functions
	bool tf1=bisecSin(a, b, maxit, root, iters);
	double root1 = root;
	int iters1 = iters;	
	bool tf2 = bisecQuadratic(a, b, maxit, root, iters);
	double root2 = root;
	int iters2 = iters;
	// do nice terminal outputs;
	if (tf1==1) {
		std::cout << "a single root of sin(x)=0 is " << root1 << std::endl;
		std::cout << "the cost of iterations for sin(x)=0 is " << iters1 << std::endl;
			}
	else {
		std::cout << "not found a root for sin(x)=0 in this interval" << std::endl;		
	}

	if (tf2==1) {
		std::cout << "a single root of x^2-2=0 is " << root2 << std::endl;
		std::cout << "the cost of iterations for x^2-2=0 is " << iters2 << std::endl;
	}
	else {
		std::cout << "not found a root for x^2-2=0 in this interval" << std::endl;
	}
	return 0;
	}

// implementation of the two functions
bool bisecSin(const double a, const double b, const int maxit, double &root,
	int &iters) {
	const double err = 0.000001;
	iters = 0;
	double an = a;
	double bn = b;
	double m = (an + bn) / 2;
	while (bn - an > err) {
		iters += 1;
		m = (an + bn) / 2;
		if (sin(an)*sin(m) > 0) {
			an = m;
		}
		else {
			bn = m;
		}
	}
	root = m;
	if (sin(an)*sin(bn) < 0) {
		return true;
	}
	else {
		return false;
	}
}

bool bisecQuadratic(const double a, const double b, const int maxit,
	double &root, int &iters) {
	const double err = 0.000001;
	iters = 0;
	double an = a;
	double bn = b;
	double m = (an + bn) / 2;
	while (bn - an > err) {
		iters += 1;
		m = (an + bn) / 2;
		if ((an*an-2)*(m*m-2)> 0) {
			an = m;
		}
		else {
			bn = m;
		}
	}
	root = m;
	if (((an*an-2)*(bn*bn-2)) < 0) {
		return true;
	}
	else {
		return false;
	}
}